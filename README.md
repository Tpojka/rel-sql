# rel-sql

[![Latest Unstable Version](https://poser.pugx.org/tpojka/rel-sql/v/unstable)](https://packagist.org/packages/tpojka/rel-sql)
[![Software License][ico-license]](./LICENSE.md)

Package for connecting relational databases such are MySQL or PostgreSql. Code is in development phase until reaches version v1.

## Structure

```
src/
tests/
```


## Install

Via Composer

``` bash
$ composer require tpojka/rel-sql
```

## Usage

Be sure you have created database

```
// initialize migrations within project
php vendor/bin/phinx init
// set db credentials in newly created ./phinx.php file

mkdir -p db/migrations db/seeds
php vendor/bin/phinx create MyFirstMigration
php vendor/bin/phinx migrate -e development

// further explanation of how to use phinx can be checked in docs
// @see https://book.cakephp.org/phinx/0/en/contents.html
```

After you have table set rel-sql package can be used similarly to:

``` php
use Tpojka\RelSql\MysqlAdapter;
use Tpojka\RelSql\Db;

$mysqlAdapter = new MysqlAdapter($host, $name, $user, $pass);
$db = new Db($mysqlAdapter);

$db->connect();

$db->setTable('books');

// default is `id`
$db->setPrimaryKey('hungarian_notation_id');

return $db->getAll();
```

## Testing

During tests table `test_table` will be created

``` bash
$ composer test
```

## Security

Package is in dev phase and use in production is not advisable.

## Credits

- [Goran Grbic][link-author]

## License

The MIT License (MIT). Please see [License File](./LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/tpojka/rel-sql.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/tpojka/rel-sql/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/tpojka/rel-sql.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/tpojka/rel-sql.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/tpojka/rel-sql.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/tpojka/rel-sql
[link-travis]: https://travis-ci.org/tpojka/rel-sql
[link-scrutinizer]: https://scrutinizer-ci.com/g/tpojka/rel-sql/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/tpojka/rel-sql
[link-downloads]: https://packagist.org/packages/tpojka/rel-sql
[link-author]: https://github.com/Tpojka
