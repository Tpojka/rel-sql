<?php declare(strict_types=1);
/**
 * Project rel-sql
 * File: Db.php
 * Created by: tpojka
 * On: 31. 7. 2021.
 */

namespace Tpojka\RelSql;

use Exception;
use PDO;

final class Db implements CrudAwareInterface
{
    /**
     * @var PDO|null
     */
    private ?PDO $dbh = null;

    /**
     * @var string
     */
    private string $table;

    /**
     * @var string
     */
    private string $primaryKey = 'id';

    /**
     * @param DbAdapter $dbAdapter
     */
    public function __construct(DbAdapter $dbAdapter)
    {
        $this->dbh = $dbAdapter->connect();
    }

    private function __clone()
    {
    }

    /**
     * @throws Exception
     */
    public function __wakeup()
    {
        throw new Exception("Cannot unserialize Db");
    }

    /**
     * @param $table
     * @return void
     */
    public function setTable($table): void
    {
        $this->table = $table;
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @param string|null $primaryKey
     */
    public function setPrimaryKey(?string $primaryKey = null): void
    {
        if (!is_null($primaryKey)) {
            $this->primaryKey = $primaryKey;
        }
    }

    /**
     * @return string
     */
    public function getPrimaryKey(): string
    {
        return $this->primaryKey;
    }

    /**
     * @return array|false
     * @throws Exception
     */
    public function getAll(): bool|array
    {
        if (!$this->table) {
            throw new Exception('Table not set.');
        }
        
        $sth = $this->dbh->query("SELECT * FROM " . $this->getTable());
        
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function getOne(int $id): mixed
    {
        if (!$this->table) {
            throw new Exception('Table not set.');
        }
        
        $sql = sprintf(
            "SELECT * FROM %s WHERE %s = ?",
            $this->getTable(),
            $this->getPrimaryKey()
        );
        
        $sth = $this->dbh->query($sql);
        $sth->execute([$id]);
        
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param array $data
     * @return string
     */
    public function insert(array $data): string
    {
        if (isset($data[0]) && is_array($data[0])) {
//            return $this->insertMultiple($data);
            exit;
        }

        ksort($data);
        
        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s)",
            $this->getTable(),
            implode(',', array_keys($data)),
            implode(',', array_map(function ($item) {
                return ':' . $item;
            }, array_keys($data)))
        );
        
        $sth = $this->dbh->prepare($sql);
        foreach ($data as $k => $v) {
            $sth->bindValue(":$k", $v);
        }
        
        $sth->execute();
        
        return $this->dbh->lastInsertId($this->getPrimaryKey());
    }

    private function insertMultiple($data)
    {
        $sql = 'INSERT INTO ' . $this->getTable();
        
        foreach ($data as &$row) {
            ksort($row);
            foreach ($row as $column => $value) {
            }
        }
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function update(int $id, array $data): bool
    {
        if (!$this->table) {
            throw new Exception('Table not set.');
        }
        
        ksort($data);
        
        $sql = sprintf(
            "UPDATE %s SET %s WHERE %s = %s",
            $this->getTable(),
            implode(',', array_map(function ($item) {
                return $item . ' = :' . $item;
            }, array_keys($data))),
            $this->getPrimaryKey(),
            ':' . $this->getPrimaryKey()
        );
        
        $sth = $this->dbh->prepare($sql);
        
        $sth->bindValue(":" . $this->getPrimaryKey(), $id);
        
        foreach ($data as $k => $v) {
            $sth->bindValue(":$k", $v);
        }
        $sth->execute();

        return $sth->rowCount() > 0;
    }

    /**
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function delete(int $id): bool
    {
        if (!$this->table) {
            throw new Exception('Table not set.');
        }
        
        $sql = sprintf(
            "DELETE FROM %s WHERE %s = %s;",
            $this->getTable(),
            $this->getPrimaryKey(),
            ':' . $this->getPrimaryKey()
        );
        
        $sth = $this->dbh->prepare($sql);
        $sth->bindValue(':' . $this->getPrimaryKey(), $id);
        $sth->execute();
        
        return $sth->rowCount() > 0;
    }

    /**
     * @param $query string
     * @return array
     * @throws Exception
     */
    public function query($query): array
    {
        $sth = $this->dbh->query($query);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Close connection at the end of the cycle
     */
    public function __destruct()
    {
        $this->closeConnection();
    }

    /**
     * Close connection
     */
    private function closeConnection(): void
    {
        $this->dbh = null;
    }
}
