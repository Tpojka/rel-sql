<?php
/**
 * Project rel-sql
 * File: CrudInterface.php
 * Created by: tpojka
 * On: 31. 7. 2021.
 */

namespace Tpojka\RelSql;

interface CrudAwareInterface
{
    public function getAll();
    public function getOne(int $id);
    public function insert(array $data);
    public function update(int $id, array $data): bool;
    public function delete(int $id): bool;
}
