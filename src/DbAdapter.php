<?php
/**
 * Project rel-sql
 * File: DbAdapter.php
 * Created by: tpojka
 * On: 31. 7. 2021.
 */

namespace Tpojka\RelSql;

interface DbAdapter
{
    public function connect();

    public function getDsn();
}
