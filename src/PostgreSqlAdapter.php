<?php
/**
 * Project rel-sql
 * File: PostgreSqlAdapter.php
 * Created by: tpojka
 * On: 31. 7. 2021.
 */

namespace Tpojka\RelSql;

use PDO;

class PostgreSqlAdapter implements DbAdapter
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port = 5432;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $pass;

    public function __construct($host, $name, $user, $pass, ?int $port = null)
    {
        $this->host = $host;
        $this->name = $name;
        $this->user = $user;
        $this->pass = $pass;

        if (!is_null($port)) {
            $this->port = $port;
        }
    }

    /**
     * @return PDO
     */
    public function connect(): PDO
    {
        return new PDO($this->getDsn(), $this->user, $this->pass, $options = []);
    }

    /**
     * @return string
     */
    public function getDsn(): string
    {
        return sprintf(
            "pgsql:host=%s;port=%s;dbname=%s",
            $this->host,
            $this->port,
            $this->name
        );
    }
}
