<?php

declare(strict_types=1);

namespace Tpojka\RelSql;

use Exception;
use PDO;
use PHPUnit\Framework\TestCase;

class DbConnectTest extends TestCase
{
    private $mysqlConfig = [
        'host' => 'localhost',
        'name' => 'test_database',
        'user' => 'tpojka',
        'pass' => 'pAssword!123',
    ];
    
    private $psqlConfig = [
        'host' => 'localhost',
        'name' => 'postgres',
        'user' => 'tpojka',
        'pass' => 'pAssword!123',
    ];
    
    /**
     * @test
     */
    public function can_instantiate_mysql_pdo_object()
    {
        $dbAdapter = new MysqlAdapter(...$this->mysqlConfig);
        
        $db = new Db($dbAdapter);
        
        $this->assertTrue(get_class($db) === Db::class);
    }
    
    /**
     * @test
     */
    public function can_instantiate_postgre_pdo_object()
    {
        $dbAdapter = new PostgreSqlAdapter(...$this->psqlConfig);

        $db = new Db($dbAdapter);

        $this->assertTrue(get_class($db) === Db::class);
    }

    /**
     * @test
     * @throws Exception
     */
    public function can_insert_a_row_in_database()
    {
        $this->createMysqlTestTable();
        
        $dbAdapter = new MysqlAdapter(...$this->mysqlConfig);
        
        $dbh = new Db($dbAdapter);
        
        $dbh->setTable('test_table');
        
        $insert = $dbh->insert([
            'name' => 'Goran',
            'email' => 'goran@tpojka.com',
        ]);
        
        $this->assertTrue((int)$insert > 0);
    }

    /**
     * @test
     */
    public function can_update_a_row_in_database()
    {
        $dbAdapter = new MysqlAdapter(...$this->mysqlConfig);

        $dbh = new Db($dbAdapter);

        $dbh->setTable('test_table');

        $rows = $dbh->query("SELECT id, name, email FROM test_table LIMIT 1 OFFSET 0");

        $update = $dbh->update((int)$rows[0]['id'], [
            'name' => 'Tpojka',
            'email' => 'tpojka@tpojka.com',
        ]);
        
        $this->assertTrue(true);
    }

    /**
     * @throws Exception
     */
    public function test_can_delete_a_row_from_database()
    {
        $dbAdapter = new MysqlAdapter(...$this->mysqlConfig);

        $dbh = new Db($dbAdapter);

        $dbh->setTable('test_table');

        $dbh->setTable('test_table');

        $rows = $dbh->query("SELECT id, name, email FROM test_table LIMIT 1 OFFSET 0");

        $delete = $dbh->delete((int)$rows[0]['id']);

        $this->assertTrue($delete);
    }

    /**
     * @throws Exception
     */
    private function createMysqlTestTable()
    {
        
        if (true === $this->checkTableAlreadyExists()) {
            return;
        }

        $pdo = new PDO(
            'mysql:host=localhost;dbname=test_database',
            $this->mysqlConfig['user'],
            $this->mysqlConfig['pass']
        );

        $sql = "create table test_table(
           id INT NOT NULL AUTO_INCREMENT,
           name VARCHAR(63) NOT NULL,
           email VARCHAR(255) NOT NULL,
           PRIMARY KEY (id)
        );";

        $pdo->exec($sql);
    }

    /**
     * @return bool
     */
    private function checkTableAlreadyExists(): bool
    {
        $pdo = new PDO(
            'mysql:host=localhost;dbname=test_database',
            $this->mysqlConfig['user'],
            $this->mysqlConfig['pass']
        );

        $sql = "SHOW TABLES LIKE 'test_table';";

        $sthPdo = $pdo->prepare($sql);

        $sthPdo->execute();
        
        if (is_bool($sthPdo->fetch(PDO::FETCH_ASSOC)) && false === $sthPdo->fetch(PDO::FETCH_ASSOC)) {
            return false;
        }

        return true;
    }
}
